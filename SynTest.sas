cas auto authinfo="/home/sas/.authinfo" host='sasviyahost.ec2.internal' port=5570 sessopts=(caslib=casuser timeout=1800 locale="en_US");
caslib _ALL_ assign;

/*Definindo variável DEBUG*/
%let x=1;

/* Caso ocorra erro, retiro tabela da memória*/
%macro check_for_errors;
   %if &syserr > 1 %then %do;
		proc cas;
			 table.tableexists result=r / /* 2 */
			 caslib='PUBLIC'
			 name='HMEQ';
			 run;
						table.dropTable result=r
						caslib="PUBLIC"
						name="HMEQ";
				end;
		 quit;
   %end;
%mend check_for_errors;

/*
*Verifico a sintaxe do código;
data public.HMEQ_TRATADA;
set public.HMEQ;
YOJ = YOJ +1;
run &debug.;
*/
%put &x;
%include '/bkp/sasjenkins/DataPreparation.sas';
%put &x;
*Executa macro de erro;
%check_for_errors;

%put &syserr;
